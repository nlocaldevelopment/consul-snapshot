FROM golang:1.13-alpine

MAINTAINER Daniel Prado <daniel.prado@publicar.com>

RUN apk update \
  && apk add gcc musl-dev git linux-headers make

RUN go get bitbucket.org/nlocaldevelopment/consul-snapshot

COPY docker/docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh

ENTRYPOINT [ "/docker-entrypoint.sh" ]
